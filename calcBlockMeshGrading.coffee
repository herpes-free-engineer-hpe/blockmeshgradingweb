(($) ->
  ) if jQuery?
      jQuery
    else
      undefined

precision=10

class CalcEntry
  constructor: (@id,@title,@value,{step,min}) ->
    # Default argument values
    step ?= "any"
    min ?= undefined

    console.log("Created "+@id)
    @text=$("<span class=calcEntryText>
         #{@title}
       </span>")
    @input=$("<input type='number' value='#{@value}'/>")
    if step>0
      @input.prop('step',step)
    else
      @input.prop('step','any')
    if min?
      @input.prop('min',min)
    @remark=$("<span class=calcErrorText>")

  content: ->
    $("<tr class=calcEntry id=#{@id}>").
      append($('<td>').append(@text)).
      append($('<td>').append(@input)).
      append($('<td>').append(@remark))

  set: (val) ->
    if !val
      @input.val(Number("NaN"))
      @remark.html("Undefined")
    else if val.length
      if val[0]?
        @input.val(Number(val[0].toPrecision(precision)))
      else
        @input.val(Number("NaN"))
      @remark.html(val[1])
    else
      @input.val(Number(val.toPrecision(precision)))
      @remark.html("")

  get: () ->
    Number(@input.val())

  enable: () ->
    @input.attr("disabled",false)
    @remark.html("")

  disable: () ->
    @input.attr("disabled",true)

class CalcBlockMesh
  constructor: (id) ->
    @fields =
      length: new CalcEntry("length","Total length",1,min:1e-10)
      nr:     new CalcEntry("nr","Number of cells",10,step:1,min:1)
      ratio:  new CalcEntry("ratio","Total expansion ratio",1,min:1e-10)
      cratio: new CalcEntry("cratio","Cell-to-cell expansion ratio",1,min:1e-10)
      widthA: new CalcEntry("widthA","Width of start cell",1,min:1e-10)
      widthB: new CalcEntry("widthB","Width of end cell",1,min:1e-10)

    $("#"+id).empty()
    
    tbl=$("<table class=blockMeshTable>")
    tbl.appendTo('#'+id)

    for n,field of @fields
      field.content().appendTo(tbl)

    @inputs = (
      $('<select>') for i in [1..3]
    )
    for s in @inputs
      for n,f of @fields
        $("<option>",{value: n,text:f.title}).appendTo(s)
      s.prependTo('#'+id)
    @funcs=new AllFunctions()
    for k,f of @fields
      f.input.change =>
        @changedValue()

  changedValue: () ->
    #    console.log "A value changed"
    known=(i.val() for i in @inputs)
    unknown=(n for n of @fields when n not in known)
    vals={}
    for k in known
      vals[k]=@fields[k].get()
    result=@funcs.evaluate(vals)
    for u in unknown
      v=result[u]
      #      console.log "Setting "+u+" to "+v
      @fields[u].set(v)

  setDataSelectors: (names) ->
    #    console.log "set selectors to "+names
    if names.length != @inputs.length
      console.log "Problem in setDataSelectors. Length of parameter "+
        "#{names.length} does not match number of selectors #{@inputs.length}"
      return

    for select in @inputs
      select.off('change')

    for select,i in @inputs
      select.find("option").each (index,element) ->
        name=$(element).attr("value")
        if !name
          return
        if name in names
          j=names.indexOf(name)
          if i==j
            $(element).attr('disabled',false)
          else
            $(element).attr('disabled',true)
        else
          $(element).attr('disabled',false)
      select.val(names[i]).change()

    for n,field of @fields
      if n in names
        field.enable()
      else
        field.disable()

    for select in @inputs
      select.change (event) =>
        vals=(i.val() for i in @inputs)
        @setDataSelectors vals

    @changedValue()

relTol = 1e-5
rMax=1e5

class AFunction
  # encapsulate a function with the information what it calculates
  # and what it needs

  constructor: (@args,@result,@function) ->
    # set arguments

  tryEvaluation: (known) ->
    if @result of known
      return [false,known]
    aVals=[]
    for a in @args
      if a of known
        if known[a].length
          aVals.push known[a][0]
        else
          aVals.push known[a]
      else
        return [false,known]
    tmp=@function aVals...
    if tmp?
      known[@result]=tmp
      [true,known]
    else
      [false,known]

  sumCells: (x0,r,n) ->
    if n<1
      0
    else
      len=0
      x=x0
      for i in [1..n]
        len+=x
        x*=r
      len

  rootByBisection: (f,x1,x2) ->
    maxSteps=500
    f1=f(x1)
    f2=f(x2)
    if f1*f2>=0
      console.log "#{x1} and #{x2} do not bracket the root (#{f1},#{f2})"
      return [undefined,"Internal error: Wrong start values for root finding"]
    steps=0
    while steps<maxSteps
      steps++
      xMid=0.5*(x1+x2)
      if (x2-x1)<relTol
        return xMid
      fMid=f(xMid)
      if f1*fMid<0
        x2=xMid
        f2=fMid
      else
        x1=xMid
        f1=fMid
    [undefined,"Internal error: root finding did not converge"]

  round: (val) ->
    return [Math.round(val),Number(val.toPrecision(precision))]

class AllFunctions
  # all functions
  constructor: () ->
    @functions = [
      new AFunction(
        ["nr","cratio"],"ratio",
        (nr,cratio) ->
           if nr>1
             Math.pow(cratio,nr-1)
           else
             [cratio,"Number of cells must be >1"]
      )
      new AFunction(
        ["nr","ratio"],"cratio",
        (nr,ratio) ->
           if nr>1
             Math.pow(ratio,1/(nr-1))
           else
             [ratio,"Number of cells must be >1"]
      )
      new AFunction(
        ["widthA","widthB"],"ratio",
        (widthA,widthB) ->
          widthB/widthA
      )
      new AFunction(
        ["widthA","ratio"],"widthB",
        (widthA,ratio) ->
          widthA*ratio
      )
      new AFunction(
        ["widthB","ratio"],"widthA",
        (widthB,ratio) ->
          widthB/ratio
      )
      new AFunction(
        ["widthA","cratio","nr"],"length",
        (widthA,cRatio,nr) ->
          @sumCells(widthA,cRatio,nr)
      )
      new AFunction(
        ["widthB","cratio","nr"],"length",
        (widthB,cRatio,nr) ->
          @sumCells(widthA,1/cRatio,nr)
      )
      new AFunction(
        ["ratio","cratio"],"nr",
        (ratio,cRatio) ->
          h=cRatio-1
          if Math.abs(h)>1e-5
            @round(Math.log(ratio)/Math.log(cRatio)+1)
          else
            [undefined,"Cell to cell ratio must not be 1"]
      )
      new AFunction(
        ["nr","cratio","length"],"widthA",
        (nr,cRatio,length) ->
          h=cRatio-1
          if Math.abs(h)>1e-5
            length*(1-cRatio)/(1-Math.pow(cRatio,nr))
          else
            length/nr
      )
      new AFunction(
        ["cratio","widthA","length"],"nr",
        (cratio,widthA,length) ->
          if Math.abs(cratio-1)>relTol
            @round(Math.log(1-length/widthA*(1-cratio))/Math.log(cratio))
          else
            @round(length/widthA)
      )
      new AFunction(
        ["cratio","widthB","length"],"nr",
        (cratio,widthB,length) ->
          if Math.abs(cratio-1)>relTol
            @round(Math.log(1/(1+length/widthB*(1-cratio)/cratio))/Math.log(cratio))
          else
            @round(length/widthB)
      )
      new AFunction(
        ["nr","widthB","length"],"cratio",
        (nr,widthE,length) ->
          if Math.abs(nr*widthE-length)/length < relTol
            1
          else
            if nr*widthE>length
              cMax=Math.pow(rMax,1/(nr-1))
              cMin=Math.pow(1+relTol,1/(nr-1))
            else
              cMax=Math.pow(1-relTol,1/(nr-1))
              cMin=Math.pow(1/rMax,1/(nr-1))
            @rootByBisection(
              (c) ->
                (1/Math.pow(c,nr-1))*(1-Math.pow(c,nr))/(1-c)-length/widthE
              ,cMin,cMax
            )
      )
      new AFunction(
        ["nr","widthA","length"],"cratio",
        (nr,widthA,length) ->
          if Math.abs(nr*widthA-length)/length < relTol
            1
          else
            if nr*widthA<length
              cMax=Math.pow(rMax,1/(nr-1))
              cMin=Math.pow(1+relTol,1/(nr-1))
            else
              cMax=Math.pow(1-relTol,1/(nr-1))
              cMin=Math.pow(1/rMax,1/(nr-1))
            @rootByBisection(
              (c) ->
                (1-Math.pow(c,nr))/(1-c)-length/widthA
              ,cMin,cMax
            )
      )
      new AFunction(
        ["ratio","widthA","length"],"nr",
        (ratio,widthA,length) ->
          rMax=1e5
          if ratio>1
            dMin=widthA
          else
            dMin=widthA*ratio
          if Math.abs(ratio-1)< relTol
            @round(length/dMin)
          else
            @round(
              @rootByBisection(
                (n) ->
                  (
                    (1-Math.pow(ratio,n/(n-1))) /
                    (1-Math.pow(ratio,1/(n-1)))
                  )-length/widthA
                ,0,length/dMin
              )
            )
      )
    ]

  evaluate: (vals) ->
    goOn = true
    while goOn
      goOn=false
      for f in @functions
        [ok,vals]=f.tryEvaluation(vals)
        if ok
          goOn=true
          break
    vals

runTest= (vals) ->
  keys=(k for k of vals)
  funcs=new AllFunctions()
  console.log("Keys: "+keys)
  total=0
  ok=0
  for i in [0..keys.length-3]
    for j in [i+1..keys.length-2]
      for l in [j+1..keys.length-1]
        total++
        known=[keys[i],keys[j],keys[l]]
        unknown=(k for k in keys when k not in known)
        console.log "Known: "+known+" Unknown:"+unknown
        para={}
        for k in known
          para[k]=vals[k]
        result=funcs.evaluate(para)
        good=true
        for u in unknown
          if u not of result
            console.log "  "+u+" not calculated"
            good=false
          else if not result[u]?
            console.log "  "+u+" undefined"
            good=false
          else if relTol<Math.abs(result[u]-vals[u])
            console.log "  Wrong result for "+u+":"+result[u]
            good=false
          # else
          #   console.log u+" : "+result[u]
        if good
          ok++

  console.log(ok+" of "+total+" combinations tested OK")

if ! $?
  console.log "jQuery not available. No blockMeshGrading available"
else
  $ ->
    console.log "Starting blockMeshGrading"
    calc=new CalcBlockMesh('blockMeshGrading')
    console.log("CalcBlockMesh Ready")
    initial=["nr","length","ratio"]
    calc.setDataSelectors(initial)

window.AllFunctions=AllFunctions

window.cbmTest= () ->
  runTest( {
    ratio: 1
    cratio: 1
    nr: 10
    widthA:1
    widthB:1
    length:10
  } )
  runTest( {
    ratio: 4
    cratio: 2
    nr: 3
    widthA:1
    widthB:4
    length:7
  } )
  runTest( {
    ratio: 1/4
    cratio: 1/2
    nr: 3
    widthA:4
    widthB:1
    length:7
  } )
