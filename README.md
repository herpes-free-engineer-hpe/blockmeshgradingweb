# Description

This script is a utility to calculate the grading factor for the
`blockMesh` utility in http://www.openfoam.org

The "official" installation of the script is found [here on openfoamwiki.net](http://openfoamwiki.net/index.php/Scripts/blockMesh_grading_calculation)

A demonstration can be found [here](http://bgschaid.bitbucket.org/blockMeshGradingWeb/)

# Installation/Usage

The actual source code is the [CoffeeScript](http://coffeescript.org/)
source. To use it it has to be compiled to JavaScript. The JavaScript-
code has to be included on the page in question and a `div`-element
with a `id="blockMeshGrading"` has to be inserted in the HTML-code.
The script will generate the necessary GUI-elements there. For an
example see the `testPage.html`

The script assumes that [jQuery library](http://jquery.com) is loaded

# License

This project is licensed under the [GNU AGPL](http://www.gnu.org/licenses/agpl-3.0.html)